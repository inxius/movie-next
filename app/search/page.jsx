import React from 'react'
import { Fragment } from 'react'
import Gallery from '../components/gallery'

async function fetchSearch(searchKeyword) {
    const keyword = processKeyword(searchKeyword)
    const key = process.env.KEY;
	const host = process.env.HOST;
	const url = `https://moviesdatabase.p.rapidapi.com/titles/search/keyword/${keyword}?startYear=2000&limit=50&endYear=2020`;
	const options = {
		method: 'GET',
		headers: {
			'X-RapidAPI-Key': key,
			'X-RapidAPI-Host': host,
		}
	}
	const res = await fetch(url, options);
	return res.json();
}

function processKeyword(keywords) {
    const keyword = keywords.split(' ')
    return keyword[0].toLowerCase()
}

export default async function Search({ searchParams }) {
    const searchData = await fetchSearch(searchParams.key)
    return (
        <Fragment>
			<div className='min-h-screen w-screen bg-slate-800'>
                <div className='py-8 flex flex-col justify-center items-center'>
                    <p className='font-bold text-2xl text-orange-500'>{'Your search for ' + searchParams.key + ':'}</p>
                </div>
                {!searchData.results.length && (
                    <Fragment>
                        <div>
                        <div className='py-8 flex flex-col justify-center items-center'>
                            <p className='font-bold text-2xl text-orange-500'>Not found!</p>
                        </div>
                        </div>
                    </Fragment>
                )}
				<div className='grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-x-2 gap-y-4 justify-items-center content-start md:content-end py-8'>
					{searchData.results.map((movie) => 
					(
						<Gallery
							targetKey={movie.id}
							targetUrl={"/info/"+movie.id}
							imgUrl={movie.primaryImage ? movie.primaryImage.url : '/icon/no-image.svg'}
							releaseYear={movie.releaseYear?.year}
							title={movie.originalTitleText?.text}
						/>
					))}
				</div>
			</div>
		</Fragment>
    )
}
