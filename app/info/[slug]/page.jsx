import React from 'react'
import { Fragment } from 'react'
import Image from 'next/image'
import Link from 'next/link'
import { notFound } from 'next/navigation'

async function fetchMovieBaseInfo(movieId) {
    const key = process.env.KEY;
	const host = process.env.HOST;
	const url = `https://moviesdatabase.p.rapidapi.com/titles/${movieId}?info=base_info`;
	const options = {
		method: 'GET',
		headers: {
			'X-RapidAPI-Key': key,
			'X-RapidAPI-Host': host,
		}
	}
	const res = await fetch(url, options);
	return res.json();
}

async function fetchMovieWriter(movieId) {
    const key = process.env.KEY;
	const host = process.env.HOST;
	const url = `https://moviesdatabase.p.rapidapi.com/titles/${movieId}?info=creators_directors_writers`;
	const options = {
		method: 'GET',
		headers: {
			'X-RapidAPI-Key': key,
			'X-RapidAPI-Host': host,
		}
	}
	const res = await fetch(url, options);
	return res.json();
}

async function fetchMovieCast(movieId) {
    const key = process.env.KEY;
	const host = process.env.HOST;
	const url = `https://moviesdatabase.p.rapidapi.com/titles/${movieId}?info=extendedCast`;
	const options = {
		method: 'GET',
		headers: {
			'X-RapidAPI-Key': key,
			'X-RapidAPI-Host': host,
		}
	}
	const res = await fetch(url, options);
	return res.json();
}

export default async function Info({ params }) {
    const getMovieBaseInfo = fetchMovieBaseInfo(params.slug)
    const getMovieWriter = fetchMovieWriter(params.slug)
    const getMovieCast = fetchMovieCast(params.slug)

    const [movieBaseInfo, movieWriter, movieCast] = await Promise.all([getMovieBaseInfo, getMovieWriter, getMovieCast])

    if (movieBaseInfo.results === null) {
        notFound()
    }

    return (
        <Fragment>
            <div className='min-h-screen w-screen bg-slate-800 px-4 py-8 md:px-8 md:py-4'>
                <div className='grid grid-cols-1 md:grid-cols-12 gap-4 justify-items-center content-start'>
                    <div className='grid grid-cols-1 md:col-start-2 md:col-span-4 gap-0'>
                        <Image 
                            src={movieBaseInfo.results.primaryImage ? movieBaseInfo.results.primaryImage.url : '/icon/no-image.svg'}
                            alt='poster-img'
                            className=''
                            quality={25}
                            width={0}
                            height={0}
                            sizes="100vw"
                            style={{ width: '100%', height: 'auto' }}
                        />
                    </div>

                    <div className='h-fit grid grid-cols-1 gap-4 md:col-span-6 md:col-end-12'>
                        <div className='grid grid-cols-1 gap-1 content-start'>
                            <p className="font-bold text-xl md:text-3xl text-orange-300"> {movieBaseInfo.results.originalTitleText?.text} </p>
                            <p className="font-bold text-base text-orange-300"> -- {movieBaseInfo.results.releaseYear?.year} -- </p>
                            <p className='font-normal text-sm text-left text-orange-300'>{movieBaseInfo.results.plot?.plotText.plainText}</p>
                            <div className='h-fit flex flex-row justify-start gap-2 overflow-x-auto py-2'>
                                {movieBaseInfo.results.genres.genres.map((genre) => (
                                    <Fragment key={genre.id}>
                                        <Link href={"/genre/"+genre.id} prefetch={false} className='bg-orange-500 h-fit py-1 px-2 rounded-md'>
                                            <p className='font-semibold text-base text-stone-900'>{genre.text}</p>
                                        </Link>
                                    </Fragment>
                                ))}
                            </div>
                        </div>

                        <div className='grid grid-cols-1 gap-1 md:grid-cols-2'>
                            <div className='grid grid-cols-3 gap-2 justify-self-start content-start'>
                                <p className='font-normal text-sm text-orange-300'>Director: </p>
                                <div className='col-span-2 grid grid-cols-1 justify-items-start content-start'>
                                    {!movieWriter.results.directors.length ? (<p className='font-normal text-sm text-orange-300'>-</p>) : movieWriter.results.directors[0].credits.map((director) => (
                                        <Fragment key={director.name.id}>
                                            <p className='font-normal text-sm text-orange-300'>- {director.name?.nameText.text}</p>
                                        </Fragment>
                                    ))}
                                </div>
                            </div>
                            <div className='grid grid-cols-3 gap-2 justify-self-start content-start'>
                                <p className='font-normal text-sm text-orange-300'>Writers: </p>
                                <div className='col-span-2 grid grid-cols-1 justify-items-start content-start'>
                                    {!movieWriter.results.writers.length ? (<p className='font-normal text-sm text-orange-300'>-</p>) : movieWriter.results.writers[0].credits.map((writer) => (
                                        <Fragment key={writer.name.id}>
                                            <p className='font-normal text-sm text-orange-300'>- {writer.name?.nameText.text}</p>
                                        </Fragment>
                                    ))}
                                </div>
                            </div>
                        </div>

                        <div className='grid grid-cols-1 gap-1'>
                            <p className='font-normal text-sm text-orange-300'>Cast:</p>
                            <div className='h-fit inline-flex w-full gap-2 overflow-x-auto py-2'>
                                {movieCast.results.cast.edges.map((cast) => (
                                    <Fragment key={cast.node.name.id}>
                                    <div className='flex flex-col gap-2'>
                                        <div className='w-36'>
                                            <Image 
                                                src={cast.node.name.primaryImage ? cast.node.name.primaryImage.url : '/icon/no-image.svg'}
                                                alt='poster-img'
                                                className='rounded-full'
                                                quality={25}
                                                width={0}
                                                height={0}
                                                sizes="100vw"
                                                style={{ width: '100%', height: '9rem', objectFit: 'cover' }}
                                            />
                                        </div>
                                        <div className='w-36'>
                                            <p className='text-center font-normal text-sm text-orange-300'>{cast.node.characters[0].name}</p>
                                            <p className='text-center font-semibold text-sm text-orange-300'>{cast.node.name.nameText.text}</p>
                                        </div>
                                    </div>
                                    </Fragment>
                                ))}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}
