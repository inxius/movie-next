import React from 'react'
import { Fragment } from 'react'
import Image from 'next/image'
import Link from 'next/link'

export default function Gallery({ targetKey, targetUrl , imgUrl, releaseYear, title }) {
    return (
        <Fragment key={targetKey}>
            <Link href={targetUrl} prefetch={false} className='self-end'>
                <div className='grid grid-cols-1 gap-0'>
                    <div className='relative w-64 h-max'>
                        <Image 
                            src={imgUrl}
                            alt='poster-img'
                            className=''
                            quality={50}
                            width={0}
                            height={0}
                            sizes="100vw"
                            style={{ width: '100%', height: 'auto' }}
                        />
                    </div>
                    <div className='w-full h-fit bg-orange-600 rounded-b-md'>
                        <div className='grid grid-cols-1 py-2 px-2 divide-y-2 gap-1 divide-slate-800'>
                            <p className="font-bold text-base text-stone-900">-- {releaseYear} --</p>
                            <div className='overflow-hidden'>
                                <p className="truncate w-56 font-semibold text-base text-stone-900">{title}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </Link>
        </Fragment>
    )
}
