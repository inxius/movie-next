'use client'
import React from 'react'
import { Fragment, useState, useCallback } from 'react'
import Image from 'next/image'
import Link from 'next/link'
import { useRouter, useSearchParams } from 'next/navigation'

export default function Template({ children }) {
    const [menu, setMenu] = useState(false)
    const [searchText, setSearchText] = useState('')
    const route = useRouter()
    const searchParams = useSearchParams()

    const genres = [
        {id: "Action", name:'Action'},
        {id: "Adventure", name:'Adventure'},
        {id: "Animation", name:'Animation'},
        {id: "Biography", name:'Biography'},
        {id: "Comedy", name:'Comedy'},
        {id: "Crime", name:'Crime'},
        {id: "Documentary", name:'Documentary'},
        {id: "Drama", name:'Drama'},
        {id: "Family", name:'Family'},
        {id: "Fantasy", name:'Fantasy'},
        {id: "Film-Noir", name:'Film-Noir'},
        {id: "Game-Show", name:'Game-Show'},
        {id: "History", name:'History'},
        {id: "Horror", name:'Horror'},
        {id: "Music", name:'Music'},
        {id: "Musical", name:'Musical'},
        {id: "Mystery", name:'Mystery'},
        {id: "News", name:'News'},
        {id: "Reality-TV", name:'Reality-TV'},
        {id: "Romance", name:'Romance'},
        {id: "Sci-Fi", name:'Sci-Fi'},
        {id: "Sport", name:'Sport'},
        {id: "Talk-Show", name:'Talk-Show'},
        {id: "Thriller", name:'Thriller'},
        {id: "War", name:'War'},
        {id: "Western", name:'Western'},
    ]

    const search = () => {
        if (searchText !== '') {
            // console.log(createQueryString('key', searchText));
            route.push('/search?' + createQueryString('key', searchText))
        }
        setMenu(false);
    }

    // Get a new searchParams string by merging the current
    // searchParams with a provided key/value pair
    const createQueryString = useCallback(
        (name, value) => {
            const params = new URLSearchParams(searchParams)
            params.set(name, value)
        
            return params.toString()
        },
        [searchParams]
    )

    return (
        <Fragment>
        <div onClick={() => {setMenu(!menu)}} className='fixed cursor-pointer w-fit inline-flex bg-orange-500 md:top-5 left-5 rounded-b-full md:rounded-full p-2 z-30'>
            <div className='relative w-8 h-8'>
                <Image
                    src={menu ? "/icon/cross.svg" : "/icon/burger-menu.svg"}
                    alt="Menu Button"
                    className="object-cover"
                    fill={true}
                    priority
                />
            </div>
        </div>

        <div className={menu ? 'fixed w-screen h-screen bg-orange-400 transition duration-300 ease-out translate-x-0 z-20' : 'fixed w-screen h-screen bg-orange-400 transition duration-300 ease-out -translate-x-full z-20'}>
            <div className='grid grid-cols-1 justify-items-center content-start pt-16 md:pt-4'>
                <div className='w-fit md:w-3/6'>
                    <div className='py-4 flex flex-col md:flex-row gap-0'>
                        <input 
                            type="text"
                            className='w-full border-2 border-orange-500 focus:border-orange-600 outline-none rounded-t-md md:rounded-none md:rounded-l-md py-1 px-2'
                            onChange={event => {
                                setSearchText(event.currentTarget.value)
                            }}
                        />
                        <button onClick={() => {search()}} className='bg-orange-500 md:hover:bg-orange-600 py-1 px-2 rounded-b-md md:rounded-none md:rounded-r-md'>
                            <p className='text-lg text-stone-900 font-semibold'>Search</p>
                        </button>
                    </div>
                    <div className='py-4'>
                        <p className='text-lg text-stone-900 font-bold'>Genres</p>
                        <div className='grid grid-cols-2 gap-1'>
                            {genres.map((g) => (
                                <Fragment key={g.id}>
                                    <Link className='bg-orange-500 md:hover:bg-orange-600 py-1 px-2 rounded-md' href={"/genre/"+g.id} prefetch={false} onClick={() => {setMenu(false)}}>
                                        <p className='text-sm text-stone-900 font-semibold'>{g.name}</p>
                                    </Link>
                                </Fragment>
                            ))}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {children}
        </Fragment>
    )
}
