import React from 'react'
import { Fragment } from 'react'
import Image from 'next/image'

export default function Loading() {
    return (
        <Fragment>
            <div className='min-h-screen w-screen flex justify-center items-center bg-slate-800'>
                <div className='flex flex-col justify-center items-center animate-pop-up'>
                    <div className='relative w-56 h-56'>
                        <Image
                            src="/icon/movie-loading.svg"
                            alt="Menu Button"
                            className="object-cover"
                            fill={true}
                            priority
                        />
                    </div>
                    <p className='font-bold text-2xl text-orange-500'>Loading...</p>
                </div>
            </div>
        </Fragment>
    )
}
