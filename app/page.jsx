import Image from 'next/image'
import { Fragment } from 'react'
import Link from 'next/link'
import Gallery from './components/gallery'

async function fetchMovies() {
	const key = process.env.KEY;
	const host = process.env.HOST;
	const url = 'https://moviesdatabase.p.rapidapi.com/titles?list=most_pop_movies&limit=50&startYear=2010&endYear=2020';
	const options = {
		method: 'GET',
		headers: {
			'X-RapidAPI-Key': key,
			'X-RapidAPI-Host': host,
		}
	}
	const res = await fetch(url, options);
	return res.json();
}

export default async function Home() {
	const movieData = await fetchMovies();
	return (
		<Fragment>
			<div className='min-h-screen w-screen bg-slate-800'>
				<div className='grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-x-2 gap-y-4 justify-items-center content-start md:content-end py-8'>
					{movieData.results.map((movie) => 
					(
						<Gallery
							targetKey={movie.id}
							targetUrl={"/info/"+movie.id}
							imgUrl={movie.primaryImage ? movie.primaryImage.url : '/icon/no-image.svg'}
							releaseYear={movie.releaseYear?.year}
							title={movie.originalTitleText?.text}
						/>
					))}
				</div>
			</div>
		</Fragment>
	)
}
