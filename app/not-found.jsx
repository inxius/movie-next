import React from 'react'
import { Fragment } from 'react'
import Link from 'next/link'
import Image from 'next/image'

export default function NotFound() {
    return (
        <Fragment>
            <div className='min-h-screen w-screen flex justify-center items-center bg-slate-800'>
                <div className='flex flex-col gap-4 justify-center items-center'>
                    <div className='relative w-56 h-56'>
                        <Image
                            src="/icon/not-found.svg"
                            alt="Menu Button"
                            className="object-cover"
                            fill={true}
                            priority
                        />
                    </div>
                    <p className='font-bold text-2xl text-orange-500'>Sorry, there some problem</p>
                    <Link href="/" prefetch={false} className='bg-orange-500 hover:bg-orange-600 h-fit py-1 px-2 rounded-md'>
                        <p className='font-semibold text-base text-stone-900'>Back to home</p>
                    </Link>
                </div>
            </div>
        </Fragment>
    )
}
